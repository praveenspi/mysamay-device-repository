import { Module } from '@nestjs/common';
import { DbModule } from '@neb-sports/mysamay-db-provider';
import { DeviceRepository } from '../repositories/device-repository/device-repository';


@Module({
    imports: [DbModule],
    providers: [DeviceRepository],
    exports: [DeviceRepository],
})
export class DeviceRepositoryModule {}
