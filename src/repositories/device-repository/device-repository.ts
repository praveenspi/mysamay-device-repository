import { Injectable } from '@nestjs/common';
import { Collection, Db, FilterQuery, SchemaMember } from 'mongodb';

import { MongoProvider } from '@neb-sports/mysamay-db-provider';

import { Device } from "@neb-sports/mysamay-device-model";

@Injectable()
export class DeviceRepository {
    db: Db;
    collection: Collection<Device>;

    constructor(private dbProvider: MongoProvider) { }

    async getCollection() {
        if (!this.collection) {
            this.db = await this.dbProvider.getDB(null);
            this.collection = this.db.collection(Device.collectionName);
        }
        return this.collection;
    }

    async disconnect() {
        if (this.dbProvider.client && this.dbProvider.client.isConnected()) {
            await this.dbProvider.client.close(true);
        }
    }

    async createDevice(device: Device): Promise<Device> {
        await this.getCollection();
        let result = await this.collection.insertOne(device);
        if (result.insertedCount === 1) return result.ops[0];
        return undefined;
    }

    async updateDevice(device: Device): Promise<Device> {
        await this.getCollection();
        let result = await this.collection.findOneAndReplace(
            { _id: device._id },
            device,
            { returnOriginal: false },
        );
        if (result.ok === 1) return result.value;
        else return undefined;
    }

    async getDeviceByAnyQuery(
        query: FilterQuery<Device>,
        projection: SchemaMember<Device, any>,
    ): Promise<Device> {
        try {
            await this.getCollection();
            let result = await this.collection.findOne(query, {
                projection: projection,
            });
            if (result) return result;
            else return undefined;
        } catch (error) {
            Promise.reject(error);
        }
    }

    async countDeviceByAnyQuery(
        query: FilterQuery<Device>,
    ): Promise<number> {
        try {
            await this.getCollection();
            return await this.collection.countDocuments(query);
        } catch (error) {
            Promise.reject(error);
        }
    }

    async getDeviceByAnyQueryPaginated(
        query: FilterQuery<Device>,
        skip: number,
        limit: number
        ): Promise<Device[]> {
        try {
            await this.getCollection();
            let result = await this.collection.find(query).skip(skip).limit(limit).toArray();
            if (result) return result;
            else return undefined;
        } catch (error) {
            Promise.reject(error);
        }
    }

    async deleteDeviceByAnyQuery(query: FilterQuery<Device>) {
        await this.getCollection();
        await this.collection.deleteMany(query);
    }
}
