import { DbModule } from '@neb-sports/mysamay-db-provider';
import { Test, TestingModule } from '@nestjs/testing';
import { DeviceRepository } from "../device-repository/device-repository";


describe('EventsRepository', () => {
    let provider: DeviceRepository;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [DbModule],
            providers: [DeviceRepository],
        }).compile();

        provider = module.get<DeviceRepository>(DeviceRepository);
    });

    it('should be defined', () => {
        expect(provider).toBeDefined();
    });

});
